using System;
using System.Collections.Generic;
using System.Text;

namespace LV5Z1i2kompozit
{
    interface IShipable
    {
        double Price { get; }
        double Weight { get; }
        string Description(int depth = 0);
    }
}
