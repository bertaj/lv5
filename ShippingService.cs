using System;
using System.Collections.Generic;
using System.Text;

namespace LV5Z1i2kompozit
{
    class ShippingService
    {
        private double priceForWeight;

        public ShippingService(double price = 0)
        {
            this.priceForWeight = price;
        }

        public double ShippingPrice(IShipable item)
        {
            return item.Weight * priceForWeight;
        }
    }
}
